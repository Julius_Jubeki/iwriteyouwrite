package de.jubeki;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.cmdframework.Command;

public class IwywReloadCommand extends Command {

	public IwywReloadCommand() {
		super("reload", 0, 0, "iwyw.reload");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		cs.sendMessage("�cReloading IWriteYouWrite-Plugin...");
		try {
			IWriteYouWrite.reload();
			cs.sendMessage("�aIWriteYouWrite has been reloaded!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured!");
			cs.sendMessage("�cPlease look into the console for more information");
		}
		return true;
	}

}
