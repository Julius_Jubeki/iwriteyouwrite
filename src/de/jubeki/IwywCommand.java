package de.jubeki;

import org.bukkit.command.CommandSender;

import de.jubeki.chat.IwywChatCommand;
import de.jubeki.cmdframework.Command;
import de.jubeki.command.IwywCmdCommand;

public class IwywCommand extends Command {

	protected IwywCommand() {
		super("iwyw", 0, 0, "iwyw.help");
		addSubCommand(new IwywVersionCommand());
		addSubCommand(new IwywCmdCommand());
		addSubCommand(new IwywChatCommand());
		addSubCommand(new IwywReloadCommand());
		setWrongUsageMessage("/iwyw");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		cs.sendMessage("�2===================[ �aIWriteYouWrite �2]===================");
		cs.sendMessage("�e/iwyw �7� Shows this help");
		cs.sendMessage("�e/iwyw version �7� Shows the version");
		cs.sendMessage("�e/iwyw chat �7� Shows the help for chat-edit commands");
		cs.sendMessage("�e/iwyw command �7� Shows the help for command-edit commands");
		cs.sendMessage("�2===================[ �aIWriteYouWrite �2]===================");
		return true;
	}

}
