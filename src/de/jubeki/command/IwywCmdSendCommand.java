package de.jubeki.command;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.IWriteYouWrite;
import de.jubeki.cmdframework.Command;

public class IwywCmdSendCommand extends Command {

	public IwywCmdSendCommand() {
		super("send", "iwyw.command.edit");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		if(args.length == 0	) {
			cs.sendMessage("�2This message will be sended:");
			cs.sendMessage(IWriteYouWrite.getMessages().getString("command.send", "�cNo message in the file!"));
			return true;
		}
		StringBuilder message = new StringBuilder();
		for(String s : args) {
			message.append(s);
			message.append(" ");
		}
		IWriteYouWrite.getMessages().set("command.send", message.substring(0, message.length()-1));
		try {
			IWriteYouWrite.getMessages().save(IWriteYouWrite.getMessagesFile());
			cs.sendMessage("�aThe sended message has been edited!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured.");
			cs.sendMessage("�cFor more information look into the console.");
		}
		return true;
	}

}
