package de.jubeki.command;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.IWriteYouWrite;
import de.jubeki.cmdframework.Command;

public class IwywCmdExecuteCommand extends Command {

	public IwywCmdExecuteCommand() {
		super("execute", 0,1, "iwyw.command.edit");
		setTabComplete(new String[] {"true", "false"});
		setWrongUsageMessage("/iwyw command execute <true/false>");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		if(args.length == 0	) {
			cs.sendMessage("�2Should the sender send the command:");
			cs.sendMessage(String.valueOf(IWriteYouWrite.getMessages().getBoolean("command.execute", false)));
			return true;
		}
		if(!args[0].toLowerCase().equals("true") && !args[0].toLowerCase().equals("false")) {
			cs.sendMessage("�cPlease write �e�ltrue �r�cor �e�lfalse");
			return true;
		}
		boolean value = Boolean.valueOf(args[0]);
		IWriteYouWrite.getMessages().set("command.execute", value);
		try {
			IWriteYouWrite.getMessages().save(IWriteYouWrite.getMessagesFile());
			cs.sendMessage("�aCommand-Execute has been edited!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured.");
			cs.sendMessage("�cFor more information look into the console.");
		}
		return true;
	}
}
