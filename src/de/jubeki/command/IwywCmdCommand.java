package de.jubeki.command;

import org.bukkit.command.CommandSender;

import de.jubeki.cmdframework.Command;

public class IwywCmdCommand extends Command {

	public IwywCmdCommand() {
		super("command", 0, 0, "iwyw.help");
		setWrongUsageMessage("/iwyw command");
		addSubCommand(new IwywCmdCheckCommand());
		addSubCommand(new IwywCmdEnableCommand());
		addSubCommand(new IwywCmdDisableCommand());
		addSubCommand(new IwywCmdSendCommand());
		addSubCommand(new IwywCmdExecuteCommand());
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		cs.sendMessage("�2===================[ �aIWriteYouWrite �2]===================");
		cs.sendMessage("�e/iwyw command �7� Shows this help");
		cs.sendMessage("�e/iwyw command enable �7� Enables the check");
		cs.sendMessage("�e/iwyw command disable �7� Disables the check");
		cs.sendMessage("�e/iwyw command check [Message] �7� Which command should be checked for");
		cs.sendMessage("�e/iwyw command send [Message] �7� Which message should be sended");
		cs.sendMessage("�e/iwyw chat execute [true/false]�7� Should player send the sended message");
		cs.sendMessage("�2===================[ �aIWriteYouWrite �2]===================");
		return true;
	}

}
