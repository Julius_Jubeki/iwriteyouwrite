package de.jubeki.command;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.IWriteYouWrite;
import de.jubeki.cmdframework.Command;

public class IwywCmdEnableCommand extends Command {

	public IwywCmdEnableCommand() {
		super("enable",0,0, "iwyw.command.enable");
		setWrongUsageMessage("/iwyw command enable");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		IWriteYouWrite.getMessages().set("command.enabled", true);
		try {
			IWriteYouWrite.getMessages().save(IWriteYouWrite.getMessagesFile());
			cs.sendMessage("�aThe command listener has been enabled!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured.");
			cs.sendMessage("�cFor more information look into the console.");
		}
		return true;
	}

}