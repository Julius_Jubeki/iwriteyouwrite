package de.jubeki.chat;

import org.bukkit.command.CommandSender;

import de.jubeki.cmdframework.Command;

public class IwywChatCommand extends Command {

	public IwywChatCommand() {
		super("chat", 0, 0, "iwyw.help");
		setWrongUsageMessage("/iwyw chat");
		addSubCommand(new IwywChatCheckCommand());
		addSubCommand(new IwywChatEnableCommand());
		addSubCommand(new IwywChatDisableCommand());
		addSubCommand(new IwywChatSendCommand());
		addSubCommand(new IwywChatExecuteCommand());
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		cs.sendMessage("�2===================[ �aIWriteYouWrite �2]===================");
		cs.sendMessage("�e/iwyw chat �7� Shows this help");
		cs.sendMessage("�e/iwyw chat enable �7� Enables the check");
		cs.sendMessage("�e/iwyw chat disable �7� Disables the check");
		cs.sendMessage("�e/iwyw chat check [Message] �7� Which message should be checked for");
		cs.sendMessage("�e/iwyw chat send [Message] �7� Which message should be sended");
		cs.sendMessage("�e/iwyw chat execute [true/false]�7� Should player send the sended message");
		cs.sendMessage("�2===================[ �aIWriteYouWrite �2]===================");
		return true;
	}

}
