package de.jubeki.chat;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.IWriteYouWrite;
import de.jubeki.cmdframework.Command;

public class IwywChatEnableCommand extends Command {

	public IwywChatEnableCommand() {
		super("enable",0,0, "iwyw.chat.enable");
		setWrongUsageMessage("/iwyw chat enable");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		IWriteYouWrite.getMessages().set("chat.enabled", true);
		try {
			IWriteYouWrite.getMessages().save(IWriteYouWrite.getMessagesFile());
			cs.sendMessage("�aThe chat listener has been enabled!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured.");
			cs.sendMessage("�cFor more information look into the console.");
		}
		return true;
	}

}