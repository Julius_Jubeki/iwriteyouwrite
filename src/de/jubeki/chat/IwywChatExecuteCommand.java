package de.jubeki.chat;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.IWriteYouWrite;
import de.jubeki.cmdframework.Command;

public class IwywChatExecuteCommand extends Command {

	public IwywChatExecuteCommand() {
		super("execute", 0,1, "iwyw.chat.edit");
		setTabComplete(new String[] {"true", "false"});
		setWrongUsageMessage("/iwyw chat execute <true/false>");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		if(args.length == 0	) {
			cs.sendMessage("�2Should the sender send the command:");
			cs.sendMessage(String.valueOf(IWriteYouWrite.getMessages().getBoolean("chat.execute", false)));
			return true;
		}
		if(!args[0].toLowerCase().equals("true") && !args[0].toLowerCase().equals("false")) {
			cs.sendMessage("�cPlease write �e�ltrue �r�cor �e�lfalse");
			return true;
		}
		boolean value = Boolean.valueOf(args[0]);
		IWriteYouWrite.getMessages().set("chat.execute", value);
		try {
			IWriteYouWrite.getMessages().save(IWriteYouWrite.getMessagesFile());
			cs.sendMessage("�aChat-Execute has been edited!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured.");
			cs.sendMessage("�cFor more information look into the console.");
		}
		return true;
	}
}
