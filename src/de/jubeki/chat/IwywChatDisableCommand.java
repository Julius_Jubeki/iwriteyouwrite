package de.jubeki.chat;

import java.io.IOException;

import org.bukkit.command.CommandSender;

import de.jubeki.IWriteYouWrite;
import de.jubeki.cmdframework.Command;

public class IwywChatDisableCommand extends Command {

	public IwywChatDisableCommand() {
		super("disable",0,0, "iwyw.chat.disable");
		setWrongUsageMessage("/iwyw chat disable");
	}

	@Override
	public boolean execute(CommandSender cs, String[] args) {
		IWriteYouWrite.getMessages().set("chat.enabled", false);
		try {
			IWriteYouWrite.getMessages().save(IWriteYouWrite.getMessagesFile());
			cs.sendMessage("�aThe chat listener has been disabled!");
		} catch (IOException e) {
			e.printStackTrace();
			cs.sendMessage("�cAn error has occured.");
			cs.sendMessage("�cFor more information look into the console.");
		}
		return true;
	}

}