package de.jubeki.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import de.jubeki.IWriteYouWrite;

public class CheckCmdListener implements Listener {
	
	boolean used = false;
	
	@EventHandler
	public void check(PlayerCommandPreprocessEvent e) {
		if(used) return;
		if(IWriteYouWrite.getMessages().getBoolean("command.enabled", true) == false) return;
		if(e.getMessage().equalsIgnoreCase(IWriteYouWrite.getMessages().getString("command.check", "�cNo check found!")) == false) return;
		if(e.getPlayer().hasPermission("iwyw.command.use") == false) return;
		used = true;
		if(IWriteYouWrite.getMessages().getBoolean("command.execute", false)) {
			e.getPlayer().chat(IWriteYouWrite.getMessages().getString("command.check", "�cNo check found!"));
		}
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p == e.getPlayer() || p.hasPermission("iwyw.prevent")) continue;
			p.chat(IWriteYouWrite.getMessages().getString("command.send", "�cNo message found!"));
		}
		e.setCancelled(true);
		used = false;
	}

}
