package de.jubeki.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.jubeki.IWriteYouWrite;

public class CheckChatListener implements Listener {
	
	boolean used = false;
	
	@EventHandler
	public void check(AsyncPlayerChatEvent e) {
		if(used) return;
		if(IWriteYouWrite.getMessages().getBoolean("chat.enabled", true) == false) return;
		if(e.getMessage().equalsIgnoreCase(IWriteYouWrite.getMessages().getString("chat.check", "�cNo check found!")) == false) return;
		if(e.getPlayer().hasPermission("iwyw.chat.use") == false) return;
		used = true;
		if(IWriteYouWrite.getMessages().getBoolean("chat.execute", true)) {
			e.getPlayer().chat(IWriteYouWrite.getMessages().getString("chat.check", "�cNo check found!"));
		}
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p == e.getPlayer() || p.hasPermission("iwyw.prevent")) continue;
			p.chat(IWriteYouWrite.getMessages().getString("chat.send", "�cNo message found!"));
		}
		e.setCancelled(true);
		used = false;
	}
}
