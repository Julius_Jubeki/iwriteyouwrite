package de.jubeki.cmdframework;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class PlayerCommand extends Command {
	
	private String noPlayer = "�cYou are no Player!";

	public PlayerCommand(String cmd, int min, int max, String... permissions) {
		super(cmd, min, max, permissions);
	}

	public PlayerCommand(String cmd, String... permissions) {
		super(cmd, permissions);
	}
	
	@Override
	public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmd, String label, String[] args) {
		if(cs instanceof Player == false) {
			cs.sendMessage(noPlayer);
			return true;
		}
		return super.onCommand(cs, cmd, label, args);
	}
	
	@Override
	public boolean execute(CommandSender cs, String[] args) {
		return execute((Player) cs, args);
	}
	
	public abstract boolean execute(Player p, String[] args);

	public String getNoPlayerMessage() {
		return noPlayer;
	}

	public void setNoPlayerMessage(String noPlayer) {
		this.noPlayer = noPlayer;
	}
	
	@Override
	public boolean checkPermissions(CommandSender cs) {
		if(cs instanceof Player == false) return false;
		return super.checkPermissions(cs);
	}

}
