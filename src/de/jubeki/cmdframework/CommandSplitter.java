package de.jubeki.cmdframework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.command.CommandSender;


public abstract class CommandSplitter extends Command {
	
	private HashMap<Integer, Command> commands = new HashMap<>();

	@Override
	public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmd, String label, String[] args) {
		Command splitted = commands.get(args.length);
		if(splitted != null) {
			splitted.onCommand(cs, cmd, label, args);
			return true;
		}
		return super.onCommand(cs, cmd, label, args);
	}
	
	public CommandSplitter(String command, String permissions) {
		super(command, permissions);
	}

	public HashMap<Integer, Command> getCommands() {
		return commands;
	}

	public void setCommands(HashMap<Integer, Command> commands) {
		this.commands = commands;
	}
	
	public void addCommand(int argsLength, Command cmd) {
		commands.put(argsLength, cmd);
	}
	
	public void removeCommand(int argsLength) {
		commands.remove(argsLength);
	}
	
	@Override
	public ArrayList<String> onTabComplete(CommandSender cs, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		if(space && commands.size() > 0 && args.length >= 0) {
			for(Entry<Integer, Command> cmd : commands.entrySet()) {
				if(cmd.getValue().tabComplete.length < args.length) continue;
				if(cmd.getValue().checkPermissions(cs) == false) continue;
				for(String s : cmd.getValue().onTabComplete(cs, args, space)) {
					if(complete.contains(s)) continue;
					complete.add(s);
				}
			}
			return complete;
		}
		if(checkPermissions(cs) == false) return complete;
		if(args.length > tabComplete.length || args.length == tabComplete.length && space) return complete;
		int index = 0;
		if(args.length > 0 && args.length <= tabComplete.length) {
			if(space && args.length != tabComplete.length) {
				index = args.length;
			} else {
				index = args.length-1;
			}
		}
		complete.addAll(ReplaceUtil.replaceTabComplete(cs, tabComplete[index], args, space));
		return complete;
	}

}
