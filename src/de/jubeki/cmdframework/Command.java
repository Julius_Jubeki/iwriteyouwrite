package de.jubeki.cmdframework;

import java.util.ArrayList;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public abstract class Command implements CommandExecutor {

	protected final String command;
	protected String[] permissions;
	protected int min, max;
	protected String description = "";
	protected String[][] tabComplete = new String[0][0];
	
	protected String noPermission = "�cYou don't have permissions!";
	protected String wrongUsage = "�cWrong syntax!";
	protected ArrayList<Command> subcommands = new ArrayList<>();
	
	public Command(String cmd,  int min, int max, String... permissions) {
		this.command = cmd;
		this.setMin(min);
		this.setMax(max);
		this.permissions = permissions;
	}
	
	public Command(String cmd, String... permissions) {
		this(cmd, 0, -1, permissions);
	}
	
	@Override
	public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmd, String label, String[] args) {
		if(checkForSubCommands(cs, args)) return true;
		if(checkPermissions(cs) == false) {
			cs.sendMessage(noPermission);
			return true;
		}
		if(checkArguments(cs, args) == false) {
			cs.sendMessage(wrongUsage);
			return true;
		}
		if(execute(cs, args) == false) cs.sendMessage(wrongUsage);
		return true;
	}
	
	public final boolean checkArguments(CommandSender cs, String[] args) {
		return args.length >= min && (max < 0 || args.length <= max); 
	}
	
	public final boolean checkForSubCommands(CommandSender cs, String[] args) {
		if(args.length > 0) {
			for(Command cmd : subcommands) {
				if(args[0].equalsIgnoreCase(cmd.getCommand())) {
					String[] args2 = new String[args.length -1];
					System.arraycopy(args, 1, args2, 0, args.length-1);
					cmd.onCommand(cs, null, null, args2);
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean checkPermissions(CommandSender cs) {
		if(cs instanceof ConsoleCommandSender || permissions.length == 0) return true;
		for(String permission : permissions) {
			if(cs.hasPermission(permission)) return true;
		}
		return false;
	}

	public abstract boolean execute(CommandSender cs, String[] args);
	
	public void addSubCommand(Command cmd) {
		subcommands.add(cmd);
	}
	
	public void removeSubCommand(Command cmd) {
		subcommands.remove(cmd);
	}

	public String getNoPermissionMessage() {
		return noPermission;
	}

	public void setNoPermissionMessage(String noPermission) {
		if(noPermission == null || noPermission.isEmpty()) return;
		this.noPermission = noPermission;
	}

	public String getWrongUsageMessage() {
		return wrongUsage;
	}

	public void setWrongUsageMessage(String wrongUsage) {
		if(wrongUsage == null || wrongUsage.isEmpty()) return;
		this.wrongUsage = wrongUsage;
	}

	public ArrayList<Command> getSubcommands() {
		return subcommands;
	}

	public void setSubcommands(ArrayList<Command> subcommands) {
		if(subcommands == null) return;
		this.subcommands = subcommands;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description == null || description.isEmpty()) return;
		this.description = description;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		if(min < 0) min = 0;
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		if(max < 0) max = -1;
		this.max = max;
	}

	public String getCommand() {
		return command;
	}

	public String[][] getTabComplete() {
		return tabComplete;
	}

	public void setTabComplete(String[]... tabComplete) {
		this.tabComplete = tabComplete;
	}
	
	public ArrayList<String> onTabComplete(CommandSender cs, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		if(space && subcommands.size() > 0 && args.length == 0) {
			for(Command cmd : subcommands) {
				if(cmd.checkPermissions(cs) == false) continue;
				complete.add(cmd.getCommand()) ;
			}
			return complete;
		} else if(space == false && args.length == 1 && subcommands.size() > 0) {
			for(Command cmd : subcommands) {
				if(cmd.getCommand().toLowerCase().startsWith(args[0].toLowerCase())) {
					if(cmd.checkPermissions(cs) == false) continue;
					complete.add(cmd.getCommand()) ;
				}
			}
			return complete;
		} else if(args.length > 0 && subcommands.size() > 0) {
			for(Command cmd : subcommands) {
				if(cmd.getCommand().equalsIgnoreCase(args[0])) {
					if(cmd.checkPermissions(cs) == false) return complete;
					String[] args2 = new String[args.length-1];
					System.arraycopy(args, 1, args2, 0, args2.length);
					return cmd.onTabComplete(cs, args2, space);
				}
			}
			return complete;
		}
		if(checkPermissions(cs) == false) return complete;
		if(args.length > tabComplete.length || args.length == tabComplete.length && space) return complete;
		int index = 0;
		if(args.length > 0 && args.length <= tabComplete.length) {
			if(space && args.length != tabComplete.length) {
				index = args.length;
			} else {
				index = args.length-1;
			}
		}
		complete.addAll(ReplaceUtil.replaceTabComplete(cs, tabComplete[index], args, space));
		return complete;
	}

}
