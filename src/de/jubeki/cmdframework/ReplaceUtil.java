package de.jubeki.cmdframework;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReplaceUtil {
	
	private ReplaceUtil() {}

	public static ArrayList<String> replaceTabComplete(CommandSender cs, String[] strs, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		for(String s : strs) {
			complete.addAll(replaceTabComplete(cs, s, args, space));
		}
		return complete;
	}

	public static ArrayList<String> replaceTabComplete(CommandSender cs, String s, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		if(s.equalsIgnoreCase("%player")) {
			for(Player p : Bukkit.getOnlinePlayers()) {
				complete.add(p.getName());
			}
		} else if(cs instanceof Player && s.startsWith("%")) {
			if(s.equalsIgnoreCase("%locWorld")) {
				complete.add(((Player)cs).getLocation().getWorld().getName());
			} else if(s.equalsIgnoreCase("%loc")) {
				int add = 0;
				if(space) {
					if(args.length >= 2) {
						try {
							Double.parseDouble(args[args.length-2]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					} else if(args.length >= 1) {
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {}
					}
				} else {
					if(args.length >= 3) {
						try {
							Double.parseDouble(args[args.length-3]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-2]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					} else if(args.length >= 2) {
						try {
							Double.parseDouble(args[args.length-2]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					} else {
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					}
				}
				if(add == 2) {
					complete.add(String.valueOf(((Player)cs).getLocation().getBlockZ()));
				} else if(add == 1) {
					complete.add(String.valueOf(((Player)cs).getLocation().getBlockY()));
				} else {
					complete.add(String.valueOf(((Player)cs).getLocation().getBlockX()));
				}
			} else if(s.equalsIgnoreCase("%locX")) {
				complete.add(String.valueOf(((Player)cs).getLocation().getX()));
			} else if(s.equalsIgnoreCase("%locY")) {
				complete.add(String.valueOf(((Player)cs).getLocation().getY()));
			} else if(s.equalsIgnoreCase("%locZ")) {
				complete.add(String.valueOf(((Player)cs).getLocation().getZ()));
			} else {
				complete.add(s);
			}
		} else {
			if(space) {
				complete.add(s);
			} else if(args.length > 0){
				if(s.toLowerCase().startsWith(args[args.length-1].toLowerCase())) {
					complete.add(s);
				}
			}
		}
		return complete;
	}

}
