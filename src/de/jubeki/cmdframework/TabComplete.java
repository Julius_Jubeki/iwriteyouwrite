package de.jubeki.cmdframework;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import de.jubeki.IWriteYouWrite;
import de.jubeki.IwywCommand;

public class TabComplete implements Listener {
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void on(TabCompleteEvent e) {
		if(e.getSender() instanceof ConsoleCommandSender) return;
		IwywCommand cmd = IWriteYouWrite.getInstance().getCommand();
		String[] args = e.getBuffer().split(" ");
		boolean space = e.getBuffer().endsWith(" ");
		if(!e.getBuffer().startsWith("/") || args.length == 0) {
			return;
		}
		if(args.length == 1 && space == false) {
			List<String> complete = new ArrayList<>();
			for(String s : e.getCompletions()) {
				if(s.toLowerCase().startsWith("/iwriteyouwrite:")) continue;
				if(complete.contains(s)) continue;
				complete.add(s);
			}
			if(cmd.checkPermissions(e.getSender()) == false) {
				complete.remove("/" + cmd.getCommand());
			}
			e.setCompletions(complete);
			return;
		}
		
		ArrayList<String> complete = new ArrayList<>();
		String command = "/" + cmd.getCommand();
		if(command.equalsIgnoreCase(args[0])) {
			String[] args2 = new String[args.length-1];
			System.arraycopy(args, 1, args2, 0, args2.length);
			complete.addAll(cmd.onTabComplete(e.getSender(), args2, space));
		}
		e.setCompletions(complete);
	}

}
