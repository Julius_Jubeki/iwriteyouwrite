package de.jubeki.cmdframework;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public abstract class ConsoleCommand extends Command {

	public ConsoleCommand(String cmd, int min, int max) {
		super(cmd, min, max);
	}

	public ConsoleCommand(String cmd) {
		super(cmd);
	}
	
	@Override
	public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmd, String label, String[] args) {
		if(cs instanceof ConsoleCommandSender == false) {
			cs.sendMessage(getNoPermissionMessage());
			return true;
		}
		return super.onCommand(cs, cmd, label, args);
	}
	
	@Override
	public boolean execute(CommandSender cs, String[] args) {
		return execute((ConsoleCommandSender) cs, args);
	}
	
	public abstract boolean execute(ConsoleCommandSender cs, String[] args);

	@Override
	public boolean checkPermissions(CommandSender cs) {
		if(cs instanceof ConsoleCommandSender) return true;
		return false;
	}
}
