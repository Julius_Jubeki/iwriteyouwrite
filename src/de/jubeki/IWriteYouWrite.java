package de.jubeki;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import de.jubeki.cmdframework.TabComplete;
import de.jubeki.listener.CheckChatListener;
import de.jubeki.listener.CheckCmdListener;

public class IWriteYouWrite extends JavaPlugin {
	
	private IwywCommand command;
	
	private File file;
	private FileConfiguration cfg;
	
	@Override
	public void onEnable() {
		
		try {
			reload();
			
			Bukkit.getPluginManager().registerEvents(new CheckCmdListener(), this);
			Bukkit.getPluginManager().registerEvents(new CheckChatListener(), this);
			Bukkit.getPluginManager().registerEvents(new TabComplete(), this);
			
			command = new IwywCommand();
			getCommand(command.getCommand()).setExecutor(command);
			
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("[IWriteYouWrite] An error occurd while loading config.yml");
			System.err.println("[IWriteYouWrite] Please contact the author");
		}
	}
	
	public static IWriteYouWrite getInstance() {
		return JavaPlugin.getPlugin(IWriteYouWrite.class);
	}
	
	public static FileConfiguration getMessages() {
		return getInstance().cfg;
	}
	
	public static File getMessagesFile() {
		return getInstance().file;
	}
	
	public static void reload() throws IOException {
		File file = new File(getInstance().getDataFolder(), "config.yml");
		FileConfiguration cfg ;
		if(file.exists() == false) {
			file.getParentFile().mkdirs();
			file.createNewFile();
			file = new File(getInstance().getDataFolder(), "config.yml");
			cfg = YamlConfiguration.loadConfiguration(file);
			cfg.set("chat.check", "message to check");
			cfg.set("chat.send", "message or command to send by everybody");
			cfg.set("chat.enabled", true);
			cfg.set("chat.execute", true);
			cfg.set("command.check", "/command to check");
			cfg.set("command.send", "message or command to send by everybody");
			cfg.set("command.enabled", true);
			cfg.set("command.execute", false);
			cfg.save(file);
		} else {
			cfg = YamlConfiguration.loadConfiguration(file);
		}
		getInstance().file = file;
		getInstance().cfg = cfg;
	}

	public IwywCommand getCommand() {
		return command;
	}
}
